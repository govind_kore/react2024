
// var x =7;

// function getName() {
//     console.log("Hello");
// }

// getName();
// console.log(x);
// console.log(getName);




// function a() {
//     var b = 10;
//     c();
//     function c() {
//         console.log(b);
//     }
// }

// a();




// let a = 10;

// let a = 100; // Error

// console.log(a);






// {
//     var a = 10; // Global scoped
//     let b = 20; // block scoped variable
//     const c = 30; // block scoped variable

//     console.log(a);
//     console.log(b);
//     console.log(c);
// }


// {
//     var a = 10; // Global scoped
//     let b = 20; // block scoped variable
//     const c = 30; // block scoped variable

//     console.log(a);
//     console.log(b);
//     console.log(c);
// }









// ================== CLOSURE ====================

// function x() {
//     var a= 7;
//     function y() {
//         console.log(a);
//     }
//     y();
// }
// x();

// function x() {
//     var a= 7;
//     function y() {
//         console.log(a);
//     }
//     return y;
// }
// var z = x();
// // console.log(z);

// //....
// z();

// function x() {
//     var a= 7;
//     return function y() {
//         console.log(a);
//     };
// }
// var z = x();
// // console.log(z);

// //....
// z();


// function x() {
//     var a= 7;
//     function y() {
//         console.log(a);
//     };
//     a = 100;
//     return y;
// }
// var z = x();
// // console.log(z);

// //....
// z();


function z() {
    var b = 900;
    function x() {
        var a= 7;
        function y() {
            console.log(a, b);
        };
        y();
    }
    x();
}
z();
