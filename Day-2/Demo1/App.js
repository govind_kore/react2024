import React from "react";
import ReactDOM from "react-dom/client";

const parent = React.createElement(
    "div",
    {id: "parent"},
    [
        React.createElement(
            "div",
            {id: "child"},
            [
                React.createElement(
                    "h1",
                    {id: "heading"},
                    "I am a H1 Tag here..."
                ),
                React.createElement(
                    "h2",
                    {id: "heading2"},
                    "I am a Sibling H2 Tag..."
                ),
            ]
        ),
        React.createElement(
            "div",
            {id: "child1"},
            [
                React.createElement(
                    "h3",
                    {id: "heading3"},
                    "I am a H3 Tag..."
                ),
                React.createElement(
                    "h4",
                    {id: "heading4"},
                    "I am a Sibling H4 Tag..."
                ),
            ]
        )
    ]
);

// Above code is very hard to understandand or write. It will not an elegant way to write a code
// so to makemore developer friendly code there is an syntax to write React code called as 
// JSX.(JavaScript Xml)

// const heading = React.createElement("h1", {id: "heading"}, "Hello World From React 3 !!!");
const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(parent);
